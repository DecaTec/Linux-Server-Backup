[![MIT License](https://img.shields.io/badge/license-MIT-green)](https://codeberg.org/DecaTec/Linux-Server-Backup/src/branch/master/LICENSE)
[![Donate](https://img.shields.io/badge/Donate-PayPal-green.svg)](https://www.paypal.com/cgi-bin/webscr?cmd=_donations&business=jr%40decatec%2ede&lc=US&item_name=Linux%20Server%20Backup&no_note=1&no_shipping=1&currency_code=USD&bn=PP%2dDonationsBF%3abtn_donate_SM%2egif%3aNonHosted)

# Linux-Server-Backup

Backup script for Linux servers utilizing [Borg Backup](https://www.borgbackup.org).
It is based on the (German) article [Backup-Strategie für Linux Server](https://decatec.de/linux/backup-strategie-fuer-linux-server-mit-borg-backup/).

## General information

The backup is created in two steps:
- Whole applications are backed up separately into a (temporary) local backup directory. As an example, [Nextcloud](https://nextcloud.com) is backed up by the script [Nextcloud Backup Restore](https://codeberg.org/DecaTec/Nextcloud-Backup-Restore). But you could also back up any other application, e.g. Wordpress (see [Wordpress Backup-Restore](https://codeberg.org/DecaTec/Wordpress-Backup-Restore)).
- Borg Backup is used to backup pre defined directories besides the local backup directory where the application backups are stored.

**Important** After cloning or downloading the repository, you'll have to edit the script so that it fits you needs (directories, applications included in the backup, etc.). All values which need to be customized are marked with *TODO* in the script’s comments.

## Backup

Before calling the script, initialize a Borg repository using following command (using a password):
```
borg init -e repokey-blake2 /media/hdd2/server_backup/borg
```

To create a backup, simply call this script as root after your customization.
```
./server.backup.sh
```

You can also run this script with cron in order to automate the backup (In this example every night at 2 am):
```
0 2 * * * /home/user/scripts/server_backup.sh > /dev/null 2>&1

```

## Restore

You can inspect the Borg repository to find a specific backup set:
```
borg list /media/hdd2/server_backup/borg/
```

A backup set can then be mounted:
```
mkdir -p /mnt/borg
borg mount /media/hdd2/server_backup/borg::20191006_020001 /mnt/borg

```

You can then copy contents from the mounted backup set (e.g. using *cp*).